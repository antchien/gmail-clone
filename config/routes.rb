GmailClone::Application.routes.draw do
  resources :users, :only => [:create, :new, :show]
  resources :emails, :only => [:create, :new, :index]
  resource :session, :only => [:create, :destroy, :new]

  root :to => "emails#index"
end
