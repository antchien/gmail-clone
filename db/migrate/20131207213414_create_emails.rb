class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string :subject
      t.text :body
      t.integer :user_id, :null => false
      t.timestamps
    end
    
    add_index :emails, :user_id
    
  end
end
