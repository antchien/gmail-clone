class CreateEmailRecipients < ActiveRecord::Migration
  def change
    create_table :email_recipients do |t|
      t.integer :email_id, :null => false
      t.integer :user_id, :null => false
      t.timestamps
    end
    
    add_index :email_recipients, :email_id
    add_index :email_recipients, :user_id
  end
end
