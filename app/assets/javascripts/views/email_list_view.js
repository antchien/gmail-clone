GmailClone.Views.EmailListView = Backbone.View.extend({
	
	events: {
		"click li.email": "showEmail"
	},
	
	render: function() {
		var that = this;
		
		var renderedContent = JST["emails/email_list"]({emails: that.collection});
		
		// var $ul = $('<ul></ul');
		// 		that.collection.each( function(email) {
		// 			ul.append(
		// 				$('<li></li>').html( email.get("subject") )
		// 			)
		// 		});
		
		that.$el.html(renderedContent);
		return that;
	},
	
	showEmail: function(event) {
		event.preventDefault();
		alert("hey!");
		console.log( "you clicked on task #" + $(event.currentTarget).attr("data-id") + "! Good click!");
	}
	
	
});

