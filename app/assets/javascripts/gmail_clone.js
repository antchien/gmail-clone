GmailClone = window.GmailClone = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function($rootEl, authored_emails, received_emails) {
	console.log("In Javascript!");
	var emailListView = new GmailClone.Views.EmailListView( { collection: received_emails });
	
	$rootEl.html(emailListView.render().$el);
	}
};

// $(document).ready(function(){
//   GmailClone.initialize();
// });
