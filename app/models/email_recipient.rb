class EmailRecipient < ActiveRecord::Base
  attr_accessible :recipient, :email, :email_id, :user_id
  #validates :recipient, :email, presence: true

  belongs_to(
  :recipient,
  class_name: "User",
  foreign_key: :user_id,
  primary_key: :id
  )
  
  belongs_to(
  :email,
  class_name: "Email",
  foreign_key: :email_id,
  primary_key: :id
  )
  
end
