class Email < ActiveRecord::Base
  attr_accessible :author, :subject, :body, :user_id, :recipients, :email_recipient_tags
  
  validates :author, presence: true
  
  belongs_to(
    :author,
    class_name: "User",
    foreign_key: :user_id,
    primary_key: :id
  )
  
  has_many(
    :email_recipient_tags,
    class_name: "EmailRecipient",
    foreign_key: :email_id,
    primary_key: :id
  )
  
  has_many :recipients, :through => :email_recipient_tags, source: :recipient
  
end
