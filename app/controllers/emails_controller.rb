class EmailsController < ApplicationController
  before_filter :require_current_user!
  
  def new
    
  end
  
  def create
    # subject = params[:email][:subject]
    #     params[:email][:body]
    # May have to parse out parameters and set the "recipients" to the proper users
    @email = Email.new(subject: params[:email][:subject], body: params[:email][:body])
    if @email.save
      render :json => @email
    else
      render :json => @email.errors, :status => 422
    end
  end
  
  def index
    @authored_emails = current_user.authored_emails
    @received_emails = current_user.received_emails
    puts "-------------"
    puts Email.all
    puts "-------------"
    respond_to do |format|
      format.html { render :index }
      format.html { render :json => { authored_emails: @authored_emails, received_emails: @received_emails } }
    end  
  end
  
end
